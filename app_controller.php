<?php
/**CakePHP 1.3 > Altered by blAC**/

class AppController extends Controller {
	var $helpers = array(
		'Html', 
		'Form', 
		'Session'
	);

	var $components = array(
		'Session',
		'RequestHandler',
		//Add a comment to the next line to de-activate the Auth component
		//'Auth'
	);


	function beforeFilter(){

		if(!$this->RequestHandler->isAjax()):
			//Generic Naming patterns base on containing folder name
			//Change here manually if you please
			$appName = @$_SESSION['appName'];
			$tagLine = @$_SESSION['tagLine'];
			$poster  = @$_SESSION['poster'];
		
			//Send the vars to the view
			$this->set(compact('appName', 'tagLine', 'poster', 'user'));
		endif;

		/**START CUSTOM CODE HERE**/


	}

}
