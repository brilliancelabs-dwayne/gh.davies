<?php
class PaliasAclmap extends AppModel {
	var $name = 'PaliasAclmap';
	var $validate = array(
		'palias_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'aclmap_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'Access' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Palias' => array(
			'className' => 'Palias',
			'foreignKey' => 'palias_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Aclmap' => array(
			'className' => 'Aclmap',
			'foreignKey' => 'aclmap_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
