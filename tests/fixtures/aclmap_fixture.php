<?php
/* Aclmap Fixture generated on: 2013-03-12 17:43:47 : 1363110227 */
class AclmapFixture extends CakeTestFixture {
	var $name = 'Aclmap';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'db_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'AclController' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 75, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'AclAction' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 75, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'AclKey' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 150, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Track' => array('type' => 'string', 'null' => false, 'default' => '1', 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'MyISAM')
	);

	var $records = array(
		array(
			'id' => 1,
			'db_id' => 1,
			'AclController' => 'Lorem ipsum dolor sit amet',
			'AclAction' => 'Lorem ipsum dolor sit amet',
			'AclKey' => 'Lorem ipsum dolor sit amet',
			'Track' => 'Lorem ipsum dolor sit ame'
		),
	);
}
