<?php
/* PaliasAclmap Fixture generated on: 2013-03-12 17:43:51 : 1363110231 */
class PaliasAclmapFixture extends CakeTestFixture {
	var $name = 'PaliasAclmap';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'palias_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index'),
		'aclmap_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'Access' => array('type' => 'string', 'null' => false, 'default' => '0', 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Created' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'Updated' => array('type' => 'timestamp', 'null' => false, 'default' => 'CURRENT_TIMESTAMP'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'person_element' => array('column' => array('palias_id', 'aclmap_id'), 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'MyISAM')
	);

	var $records = array(
		array(
			'id' => 1,
			'palias_id' => 1,
			'aclmap_id' => 1,
			'Access' => 'Lorem ipsum dolor sit ame',
			'Created' => '2013-03-12',
			'Updated' => 1363110231
		),
	);
}
