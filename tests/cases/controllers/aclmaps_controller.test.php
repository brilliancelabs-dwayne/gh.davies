<?php
/* Aclmaps Test cases generated on: 2013-03-12 17:43:55 : 1363110235*/
App::import('Controller', 'Aclmaps');

class TestAclmapsController extends AclmapsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class AclmapsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.aclmap', 'app.db', 'app.palias', 'app.palias_aclmap');

	function startTest() {
		$this->Aclmaps =& new TestAclmapsController();
		$this->Aclmaps->constructClasses();
	}

	function endTest() {
		unset($this->Aclmaps);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
