<?php
/* Dbs Test cases generated on: 2013-03-12 17:43:55 : 1363110235*/
App::import('Controller', 'Dbs');

class TestDbsController extends DbsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class DbsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.db', 'app.aclmap', 'app.palias', 'app.palias_aclmap');

	function startTest() {
		$this->Dbs =& new TestDbsController();
		$this->Dbs->constructClasses();
	}

	function endTest() {
		unset($this->Dbs);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
