<?php
/* Paliases Test cases generated on: 2013-03-12 17:43:55 : 1363110235*/
App::import('Controller', 'Paliases');

class TestPaliasesController extends PaliasesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class PaliasesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.palias', 'app.palias_aclmap', 'app.aclmap', 'app.db');

	function startTest() {
		$this->Paliases =& new TestPaliasesController();
		$this->Paliases->constructClasses();
	}

	function endTest() {
		unset($this->Paliases);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
