<?php
/* PaliasAclmaps Test cases generated on: 2013-03-12 17:43:55 : 1363110235*/
App::import('Controller', 'PaliasAclmaps');

class TestPaliasAclmapsController extends PaliasAclmapsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class PaliasAclmapsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.palias_aclmap', 'app.palias', 'app.aclmap', 'app.db');

	function startTest() {
		$this->PaliasAclmaps =& new TestPaliasAclmapsController();
		$this->PaliasAclmaps->constructClasses();
	}

	function endTest() {
		unset($this->PaliasAclmaps);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
