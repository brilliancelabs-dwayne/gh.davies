<?php
/* PaliasAclmap Test cases generated on: 2013-03-12 17:43:52 : 1363110232*/
App::import('Model', 'PaliasAclmap');

class PaliasAclmapTestCase extends CakeTestCase {
	var $fixtures = array('app.palias_aclmap', 'app.palias', 'app.aclmap', 'app.db');

	function startTest() {
		$this->PaliasAclmap =& ClassRegistry::init('PaliasAclmap');
	}

	function endTest() {
		unset($this->PaliasAclmap);
		ClassRegistry::flush();
	}

}
