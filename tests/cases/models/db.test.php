<?php
/* Db Test cases generated on: 2013-03-12 17:43:50 : 1363110230*/
App::import('Model', 'Db');

class DbTestCase extends CakeTestCase {
	var $fixtures = array('app.db', 'app.aclmap', 'app.palias', 'app.palias_aclmap');

	function startTest() {
		$this->Db =& ClassRegistry::init('Db');
	}

	function endTest() {
		unset($this->Db);
		ClassRegistry::flush();
	}

}
