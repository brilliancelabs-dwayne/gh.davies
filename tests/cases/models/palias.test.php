<?php
/* Palias Test cases generated on: 2013-03-12 17:43:54 : 1363110234*/
App::import('Model', 'Palias');

class PaliasTestCase extends CakeTestCase {
	var $fixtures = array('app.palias', 'app.palias_aclmap', 'app.aclmap', 'app.db');

	function startTest() {
		$this->Palias =& ClassRegistry::init('Palias');
	}

	function endTest() {
		unset($this->Palias);
		ClassRegistry::flush();
	}

}
