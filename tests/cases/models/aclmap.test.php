<?php
/* Aclmap Test cases generated on: 2013-03-12 17:43:48 : 1363110228*/
App::import('Model', 'Aclmap');

class AclmapTestCase extends CakeTestCase {
	var $fixtures = array('app.aclmap', 'app.db', 'app.palias', 'app.palias_aclmap');

	function startTest() {
		$this->Aclmap =& ClassRegistry::init('Aclmap');
	}

	function endTest() {
		unset($this->Aclmap);
		ClassRegistry::flush();
	}

}
