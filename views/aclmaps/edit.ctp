<div class="aclmaps form">
<?php echo $this->Form->create('Aclmap');?>
	<fieldset>
		<legend><?php __('Edit Aclmap'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('db_id');
		echo $this->Form->input('AclController');
		echo $this->Form->input('AclAction');
		echo $this->Form->input('AclKey');
		echo $this->Form->input('Track');
		echo $this->Form->input('Palias');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Aclmap.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Aclmap.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Aclmaps', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Dbs', true), array('controller' => 'dbs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Db', true), array('controller' => 'dbs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Paliases', true), array('controller' => 'paliases', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Palias', true), array('controller' => 'paliases', 'action' => 'add')); ?> </li>
	</ul>
</div>