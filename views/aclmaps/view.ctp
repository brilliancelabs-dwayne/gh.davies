<div class="aclmaps view">
<h2><?php  __('Aclmap');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $aclmap['Aclmap']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Db'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($aclmap['Db']['id'], array('controller' => 'dbs', 'action' => 'view', $aclmap['Db']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('AclController'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $aclmap['Aclmap']['AclController']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('AclAction'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $aclmap['Aclmap']['AclAction']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('AclKey'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $aclmap['Aclmap']['AclKey']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Track'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $aclmap['Aclmap']['Track']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Aclmap', true), array('action' => 'edit', $aclmap['Aclmap']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Aclmap', true), array('action' => 'delete', $aclmap['Aclmap']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $aclmap['Aclmap']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Aclmaps', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Aclmap', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Dbs', true), array('controller' => 'dbs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Db', true), array('controller' => 'dbs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Paliases', true), array('controller' => 'paliases', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Palias', true), array('controller' => 'paliases', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Paliases');?></h3>
	<?php if (!empty($aclmap['Palias'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Description'); ?></th>
		<th><?php __('Active'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($aclmap['Palias'] as $palias):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $palias['id'];?></td>
			<td><?php echo $palias['Name'];?></td>
			<td><?php echo $palias['Description'];?></td>
			<td><?php echo $palias['Active'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'paliases', 'action' => 'view', $palias['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'paliases', 'action' => 'edit', $palias['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'paliases', 'action' => 'delete', $palias['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $palias['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Palias', true), array('controller' => 'paliases', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
