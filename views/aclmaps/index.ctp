<div class="aclmaps index">
	<h2><?php __('Aclmaps');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('db_id');?></th>
			<th><?php echo $this->Paginator->sort('AclController');?></th>
			<th><?php echo $this->Paginator->sort('AclAction');?></th>
			<th><?php echo $this->Paginator->sort('AclKey');?></th>
			<th><?php echo $this->Paginator->sort('Track');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($aclmaps as $aclmap):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $aclmap['Aclmap']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($aclmap['Db']['id'], array('controller' => 'dbs', 'action' => 'view', $aclmap['Db']['id'])); ?>
		</td>
		<td><?php echo $aclmap['Aclmap']['AclController']; ?>&nbsp;</td>
		<td><?php echo $aclmap['Aclmap']['AclAction']; ?>&nbsp;</td>
		<td><?php echo $aclmap['Aclmap']['AclKey']; ?>&nbsp;</td>
		<td><?php echo $aclmap['Aclmap']['Track']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $aclmap['Aclmap']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $aclmap['Aclmap']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $aclmap['Aclmap']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $aclmap['Aclmap']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Aclmap', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Dbs', true), array('controller' => 'dbs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Db', true), array('controller' => 'dbs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Paliases', true), array('controller' => 'paliases', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Palias', true), array('controller' => 'paliases', 'action' => 'add')); ?> </li>
	</ul>
</div>