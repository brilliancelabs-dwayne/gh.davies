<div class="paliasAclmaps index">
	<h2><?php __('Palias Aclmaps');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('palias_id');?></th>
			<th><?php echo $this->Paginator->sort('aclmap_id');?></th>
			<th><?php echo $this->Paginator->sort('Access');?></th>
			<th><?php echo $this->Paginator->sort('Created');?></th>
			<th><?php echo $this->Paginator->sort('Updated');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($paliasAclmaps as $paliasAclmap):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $paliasAclmap['PaliasAclmap']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($paliasAclmap['Palias']['id'], array('controller' => 'paliases', 'action' => 'view', $paliasAclmap['Palias']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($paliasAclmap['Aclmap']['id'], array('controller' => 'aclmaps', 'action' => 'view', $paliasAclmap['Aclmap']['id'])); ?>
		</td>
		<td><?php echo $paliasAclmap['PaliasAclmap']['Access']; ?>&nbsp;</td>
		<td><?php echo $paliasAclmap['PaliasAclmap']['Created']; ?>&nbsp;</td>
		<td><?php echo $paliasAclmap['PaliasAclmap']['Updated']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $paliasAclmap['PaliasAclmap']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $paliasAclmap['PaliasAclmap']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $paliasAclmap['PaliasAclmap']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $paliasAclmap['PaliasAclmap']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Palias Aclmap', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Paliases', true), array('controller' => 'paliases', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Palias', true), array('controller' => 'paliases', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Aclmaps', true), array('controller' => 'aclmaps', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Aclmap', true), array('controller' => 'aclmaps', 'action' => 'add')); ?> </li>
	</ul>
</div>