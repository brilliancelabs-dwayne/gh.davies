<div class="paliasAclmaps view">
<h2><?php  __('Palias Aclmap');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $paliasAclmap['PaliasAclmap']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Palias'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($paliasAclmap['Palias']['id'], array('controller' => 'paliases', 'action' => 'view', $paliasAclmap['Palias']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Aclmap'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($paliasAclmap['Aclmap']['id'], array('controller' => 'aclmaps', 'action' => 'view', $paliasAclmap['Aclmap']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Access'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $paliasAclmap['PaliasAclmap']['Access']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $paliasAclmap['PaliasAclmap']['Created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $paliasAclmap['PaliasAclmap']['Updated']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Palias Aclmap', true), array('action' => 'edit', $paliasAclmap['PaliasAclmap']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Palias Aclmap', true), array('action' => 'delete', $paliasAclmap['PaliasAclmap']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $paliasAclmap['PaliasAclmap']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Palias Aclmaps', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Palias Aclmap', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Paliases', true), array('controller' => 'paliases', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Palias', true), array('controller' => 'paliases', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Aclmaps', true), array('controller' => 'aclmaps', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Aclmap', true), array('controller' => 'aclmaps', 'action' => 'add')); ?> </li>
	</ul>
</div>
