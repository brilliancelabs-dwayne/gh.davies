<div class="paliasAclmaps form">
<?php echo $this->Form->create('PaliasAclmap');?>
	<fieldset>
		<legend><?php __('Edit Palias Aclmap'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('palias_id');
		echo $this->Form->input('aclmap_id');
		echo $this->Form->input('Access');
		echo $this->Form->input('Created');
		echo $this->Form->input('Updated');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('PaliasAclmap.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('PaliasAclmap.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Palias Aclmaps', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Paliases', true), array('controller' => 'paliases', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Palias', true), array('controller' => 'paliases', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Aclmaps', true), array('controller' => 'aclmaps', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Aclmap', true), array('controller' => 'aclmaps', 'action' => 'add')); ?> </li>
	</ul>
</div>