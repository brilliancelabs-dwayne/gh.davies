<div class="paliases view">
<h2><?php  __('Palias');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $palias['Palias']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $palias['Palias']['Name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Description'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $palias['Palias']['Description']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Active'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $palias['Palias']['Active']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Palias', true), array('action' => 'edit', $palias['Palias']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Palias', true), array('action' => 'delete', $palias['Palias']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $palias['Palias']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Paliases', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Palias', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Palias Aclmaps', true), array('controller' => 'palias_aclmaps', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Palias Aclmap', true), array('controller' => 'palias_aclmaps', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Palias Aclmaps');?></h3>
	<?php if (!empty($palias['PaliasAclmap'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Palias Id'); ?></th>
		<th><?php __('Aclmap Id'); ?></th>
		<th><?php __('Access'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($palias['PaliasAclmap'] as $paliasAclmap):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $paliasAclmap['id'];?></td>
			<td><?php echo $paliasAclmap['palias_id'];?></td>
			<td><?php echo $paliasAclmap['aclmap_id'];?></td>
			<td><?php echo $paliasAclmap['Access'];?></td>
			<td><?php echo $paliasAclmap['Created'];?></td>
			<td><?php echo $paliasAclmap['Updated'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'palias_aclmaps', 'action' => 'view', $paliasAclmap['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'palias_aclmaps', 'action' => 'edit', $paliasAclmap['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'palias_aclmaps', 'action' => 'delete', $paliasAclmap['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $paliasAclmap['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Palias Aclmap', true), array('controller' => 'palias_aclmaps', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
