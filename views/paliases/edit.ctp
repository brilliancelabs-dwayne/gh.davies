<div class="paliases form">
<?php echo $this->Form->create('Palias');?>
	<fieldset>
		<legend><?php __('Edit Palias'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('Name');
		echo $this->Form->input('Description');
		echo $this->Form->input('Active');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Palias.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Palias.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Paliases', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Palias Aclmaps', true), array('controller' => 'palias_aclmaps', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Palias Aclmap', true), array('controller' => 'palias_aclmaps', 'action' => 'add')); ?> </li>
	</ul>
</div>