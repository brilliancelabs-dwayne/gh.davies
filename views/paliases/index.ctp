<div class="paliases index">
	<h2><?php __('Paliases');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('Name');?></th>
			<th><?php echo $this->Paginator->sort('Description');?></th>
			<th><?php echo $this->Paginator->sort('Active');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($paliases as $palias):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $palias['Palias']['id']; ?>&nbsp;</td>
		<td><?php echo $palias['Palias']['Name']; ?>&nbsp;</td>
		<td><?php echo $palias['Palias']['Description']; ?>&nbsp;</td>
		<td><?php echo $palias['Palias']['Active']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $palias['Palias']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $palias['Palias']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $palias['Palias']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $palias['Palias']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Palias', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Palias Aclmaps', true), array('controller' => 'palias_aclmaps', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Palias Aclmap', true), array('controller' => 'palias_aclmaps', 'action' => 'add')); ?> </li>
	</ul>
</div>