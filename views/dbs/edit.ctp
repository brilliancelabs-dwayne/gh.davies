<div class="dbs form">
<?php echo $this->Form->create('Db');?>
	<fieldset>
		<legend><?php __('Edit Db'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('Name');
		echo $this->Form->input('Description');
		echo $this->Form->input('Active');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Db.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Db.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Dbs', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Aclmaps', true), array('controller' => 'aclmaps', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Aclmap', true), array('controller' => 'aclmaps', 'action' => 'add')); ?> </li>
	</ul>
</div>