<div class="dbs view">
<h2><?php  __('Db');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $db['Db']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $db['Db']['Name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Description'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $db['Db']['Description']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Active'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $db['Db']['Active']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Db', true), array('action' => 'edit', $db['Db']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Db', true), array('action' => 'delete', $db['Db']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $db['Db']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Dbs', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Db', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Aclmaps', true), array('controller' => 'aclmaps', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Aclmap', true), array('controller' => 'aclmaps', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Aclmaps');?></h3>
	<?php if (!empty($db['Aclmap'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Db Id'); ?></th>
		<th><?php __('AclController'); ?></th>
		<th><?php __('AclAction'); ?></th>
		<th><?php __('AclKey'); ?></th>
		<th><?php __('Track'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($db['Aclmap'] as $aclmap):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $aclmap['id'];?></td>
			<td><?php echo $aclmap['db_id'];?></td>
			<td><?php echo $aclmap['AclController'];?></td>
			<td><?php echo $aclmap['AclAction'];?></td>
			<td><?php echo $aclmap['AclKey'];?></td>
			<td><?php echo $aclmap['Track'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'aclmaps', 'action' => 'view', $aclmap['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'aclmaps', 'action' => 'edit', $aclmap['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'aclmaps', 'action' => 'delete', $aclmap['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $aclmap['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Aclmap', true), array('controller' => 'aclmaps', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
