<?php
class PaliasesController extends AppController {

	var $name = 'Paliases';

	function index() {
		$this->Palias->recursive = 0;
		$this->set('paliases', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid palias', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('palias', $this->Palias->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Palias->create();
			if ($this->Palias->save($this->data)) {
				$this->Session->setFlash(__('The palias has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The palias could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid palias', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Palias->save($this->data)) {
				$this->Session->setFlash(__('The palias has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The palias could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Palias->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for palias', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Palias->delete($id)) {
			$this->Session->setFlash(__('Palias deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Palias was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
