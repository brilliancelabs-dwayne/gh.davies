<?php
	class AutomatonsController extends AppController {
	
		var $name = 'Automatons';
		var $uses = array('Db', 'Palias');

		function fill_map($db_id = null){
			$this->autoRender = false;
			
			$db =	 $this->Db->find('first', array('recursive' => -1, 'conditions' => array(
				'Db.id' => $db_id
			)));

			//get all tables for the provided db
			$db = @$db['Db']['Name'];
			$tables = $this->_getTables($db);
			
			//grab the person aliases
			$persons = $this->Palias->find('list', array('fields' => array('id', 'Name')));			
			
			//set the davies (similar to CRUD)
			$davies = array(1=>'delete',2=>'add',3=>'view',4=>'index',5=>'edit');
			
			$maps=array();
			foreach($persons as $p):
			
				//set davies for each person alias and table	
				$i = 1;
				foreach($tables as $t):
					foreach($davies as $cred):
						if($p!='admin' && $p!='sysadmin'):
							$maps[$p][$t][$cred] = 0;
						else:
							$maps[$p][$t][$cred] = 1;
						endif;
					endforeach;
					$i++;
					
				endforeach;
				//Make the settings JSON and write to file
				
				$json = json_encode($maps[$p]);
				$fname = APP . 'app_maps' . DS. $p . ".json";
				$file = new File($fname, true);
				$file->write($json);
				
			endforeach;
		}

		function _getTables($db = null){
			if($db):
			
				$this->Db->setDatabase($db);

				$tables = $this->Db->query('show tables');

				foreach($tables as $t):
					$list[]=$t['TABLE_NAMES']['Tables_in_'.$db];
				
				endforeach;	
			
				return $list;

			endif;
		}

	}
