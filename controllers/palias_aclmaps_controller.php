<?php
class PaliasAclmapsController extends AppController {

	var $name = 'PaliasAclmaps';

	function index() {
		$this->PaliasAclmap->recursive = 0;
		$this->set('paliasAclmaps', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid palias aclmap', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('paliasAclmap', $this->PaliasAclmap->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->PaliasAclmap->create();
			if ($this->PaliasAclmap->save($this->data)) {
				$this->Session->setFlash(__('The palias aclmap has been saved', true));
				$this->redirect(array('action' => 'add'));
			} else {
				$this->Session->setFlash(__('The palias aclmap could not be saved. Please, try again.', true));
			}
		}
		$paliases = $this->PaliasAclmap->Palias->find('list');
		$aclmaps = $this->PaliasAclmap->Aclmap->find('list');
		$this->set(compact('paliases', 'aclmaps'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid palias aclmap', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->PaliasAclmap->save($this->data)) {
				$this->Session->setFlash(__('The palias aclmap has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The palias aclmap could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->PaliasAclmap->read(null, $id);
		}
		$paliases = $this->PaliasAclmap->Palias->find('list');
		$aclmaps = $this->PaliasAclmap->Aclmap->find('list');
		$this->set(compact('paliases', 'aclmaps'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for palias aclmap', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->PaliasAclmap->delete($id)) {
			$this->Session->setFlash(__('Palias aclmap deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Palias aclmap was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
