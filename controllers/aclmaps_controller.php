<?php
class AclmapsController extends AppController {

	var $name = 'Aclmaps';

	function index() {
		$this->Aclmap->recursive = 0;
		$this->set('aclmaps', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid aclmap', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('aclmap', $this->Aclmap->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Aclmap->create();
			if ($this->Aclmap->save($this->data)) {
				$this->Session->setFlash(__('The aclmap has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The aclmap could not be saved. Please, try again.', true));
			}
		}
		$dbs = $this->Aclmap->Db->find('list');
		$paliases = $this->Aclmap->Palias->find('list');
		$this->set(compact('dbs', 'paliases'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid aclmap', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Aclmap->save($this->data)) {
				$this->Session->setFlash(__('The aclmap has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The aclmap could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Aclmap->read(null, $id);
		}
		$dbs = $this->Aclmap->Db->find('list');
		$paliases = $this->Aclmap->Palias->find('list');
		$this->set(compact('dbs', 'paliases'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for aclmap', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Aclmap->delete($id)) {
			$this->Session->setFlash(__('Aclmap deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Aclmap was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
