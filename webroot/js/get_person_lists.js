$(document).ready(function(){
	function get_list(eclass,uid){

			//eclass is the model name, i.e. "Person"
			//uid is an ID from the passed model
			var url = 'get_user_for_list/'+eclass+'/'+uid;

			//Get the DOM element id of the passed class			
			var eid = '#'+$('.'+eclass).attr('id');

			$.ajax({
				'url': url,

				'beforeSend': function(){
					$(eid).parent().find('.ajdata').html('loading...');
				},
				'error': function(err){
					//console.log(err);
				},
				'success': function(data){
					$(eid).parent().find('.ajdata').html(data);
					//console.log(data);
				}
			});
	}

	/**DEFAULT Runs of the function based on the existence of certain classes**/
	if($('#PatientId').length){
		console.log('Theres a patient here');
	}else{
		console.log('Nope')
	}
	if($('.Patient').length){
		var this_id = $('.Patient').attr('id');
console.log(this_id);
		var user_id = $('.Patient').val();
		get_list('Patient',user_id,this_id);
	}
	if($('.Provider').length){
		var this_id = $('.Provider').attr('id');
		var user_id = $('.Provider').val();
		get_list('Provider',user_id,this_id);
	}


	/**CHANGE Runs of the function based on changing of particular dropdowns**/
	$('.Patient').change(function(){
		var this_id = '#'+$(this).attr('id');
		var user_id = $(this).val();
		get_list('Patient',user_id,this_id);
	});

	$('.Provider').change(function(){
		var this_id = '#'+$(this).attr('id');
		var user_id = $(this).val();
		get_list('Provider',user_id,this_id);
	});

});
